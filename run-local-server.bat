@echo off
REM Make sure this is updated with the right path to your fivem server files
SET server_path=..\Server\
ECHO %server_path%run.cmd

if exist %server_path%run.cmd (
    @echo on
    COLOR A0
    %server_path%run.cmd +exec server.cfg
	pause
) else (
    COLOR C0
    echo [ERROR] Make sure you have the right path for the run.cmd file
    pause
)